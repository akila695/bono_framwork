package com.rezgateway.automation.pojo;

public class BookingPolicy {

	private String PolicyFrom = null;
	private String PolicyTo = null;
	private String AmendmentType = null;
	private String PolicyBasedOn = null;
	private String PolicyBasedOnValue = null;
	private String CancellationType = null;
	private String StayDateRequirement = null;
	private String ArrivalRange = null;
	private int ArrivalRangeValue = 0;
	private String PolicyFee = null;
	private String NoShowBasedOn = null;
	private String NoShowBasedOnValue = null;
	private String NoShowPolicyFee = null;

	/**
	 * @return the policyFrom
	 */
	public String getPolicyFrom() {
		return PolicyFrom;
	}

	/**
	 * @return the policyTo
	 */
	public String getPolicyTo() {
		return PolicyTo;
	}

	/**
	 * @return the amendmentType
	 */
	public String getAmendmentType() {
		return AmendmentType;
	}

	/**
	 * @return the policyBasedOn
	 */
	public String getPolicyBasedOn() {
		return PolicyBasedOn;
	}

	/**
	 * @return the policyBasedOnValue
	 */
	public String getPolicyBasedOnValue() {
		return PolicyBasedOnValue;
	}

	/**
	 * @return the cancellationType
	 */
	public String getCancellationType() {
		return CancellationType;
	}

	/**
	 * @return the stayDateRequirement
	 */
	public String getStayDateRequirement() {
		return StayDateRequirement;
	}

	/**
	 * @return the arrivalRange
	 */
	public String getArrivalRange() {
		return ArrivalRange;
	}

	/**
	 * @return the arrivalRangeValue
	 */
	public int getArrivalRangeValue() {
		return ArrivalRangeValue;
	}

	/**
	 * @return the policyFee
	 */
	public String getPolicyFee() {
		return PolicyFee;
	}

	/**
	 * @return the noShowBasedOn
	 */
	public String getNoShowBasedOn() {
		return NoShowBasedOn;
	}

	/**
	 * @return the noShowBasedOnValue
	 */
	public String getNoShowBasedOnValue() {
		return NoShowBasedOnValue;
	}

	/**
	 * @return the noShowPolicyFee
	 */
	public String getNoShowPolicyFee() {
		return NoShowPolicyFee;
	}

	/**
	 * @param policyFrom
	 *            the policyFrom to set
	 */
	public void setPolicyFrom(String policyFrom) {
		PolicyFrom = policyFrom;
	}

	/**
	 * @param policyTo
	 *            the policyTo to set
	 */
	public void setPolicyTo(String policyTo) {
		PolicyTo = policyTo;
	}

	/**
	 * @param amendmentType
	 *            the amendmentType to set
	 */
	public void setAmendmentType(String amendmentType) {
		AmendmentType = amendmentType;
	}

	/**
	 * @param policyBasedOn
	 *            the policyBasedOn to set
	 */
	public void setPolicyBasedOn(String policyBasedOn) {
		PolicyBasedOn = policyBasedOn;
	}

	/**
	 * @param policyBasedOnValue
	 *            the policyBasedOnValue to set
	 */
	public void setPolicyBasedOnValue(String policyBasedOnValue) {
		PolicyBasedOnValue = policyBasedOnValue;
	}

	/**
	 * @param cancellationType
	 *            the cancellationType to set
	 */
	public void setCancellationType(String cancellationType) {
		CancellationType = cancellationType;
	}

	/**
	 * @param stayDateRequirement
	 *            the stayDateRequirement to set
	 */
	public void setStayDateRequirement(String stayDateRequirement) {
		StayDateRequirement = stayDateRequirement;
	}

	/**
	 * @param arrivalRange
	 *            the arrivalRange to set
	 */
	public void setArrivalRange(String arrivalRange) {
		ArrivalRange = arrivalRange;
	}

	/**
	 * @param arrivalRangeValue
	 *            the arrivalRangeValue to set
	 */
	public void setArrivalRangeValue(int arrivalRangeValue) {
		ArrivalRangeValue = arrivalRangeValue;
	}

	/**
	 * @param policyFee
	 *            the policyFee to set
	 */
	public void setPolicyFee(String policyFee) {
		PolicyFee = policyFee;
	}

	/**
	 * @param noShowBasedOn
	 *            the noShowBasedOn to set
	 */
	public void setNoShowBasedOn(String noShowBasedOn) {
		NoShowBasedOn = noShowBasedOn;
	}

	/**
	 * @param noShowBasedOnValue
	 *            the noShowBasedOnValue to set
	 */
	public void setNoShowBasedOnValue(String noShowBasedOnValue) {
		NoShowBasedOnValue = noShowBasedOnValue;
	}

	/**
	 * @param noShowPolicyFee
	 *            the noShowPolicyFee to set
	 */
	public void setNoShowPolicyFee(String noShowPolicyFee) {
		NoShowPolicyFee = noShowPolicyFee;
	}

}
