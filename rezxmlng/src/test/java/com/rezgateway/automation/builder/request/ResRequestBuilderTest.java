package com.rezgateway.automation.builder.request;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.rezgateway.automation.pojo.ReservationRequest;
import com.rezgateway.automation.pojo.Room;

public class ResRequestBuilderTest {

	ReservationRequest resObJ = new ReservationRequest();
	ReservationRequestBuilder builderObj = new ReservationRequestBuilder();

	
	@Before
	public void Setup() {
		
		Room r1 = new Room();
		Room r2 = new Room();
		resObJ.setUserName("TesthotelName");
		resObJ.setPassword("Akila");
		LocalDateTime date = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
		resObJ.setReservationDetailsTimeStamp(date.format(formatter));
		resObJ.setConfType("CON");
		resObJ.setTourOperatorOrderNumber("20113217T03:32:20");
		resObJ.setCheckin("24-MAR-2018");
		resObJ.setCheckout("25-MAR-2018");
		resObJ.setNoOfRooms("2");
		resObJ.setNoofNights("1");
		resObJ.setHotelCode("6109");
		resObJ.setCurrency("USD");
		resObJ.setTotal("0");
		resObJ.setTotalaTax("0");

		r1.setRoomNo("1");
		r1.setRoomCode("99171");
		r1.setRoomTypeID("3");
		r1.setRatePlanCode("Room Only");
		r1.setBedTypeID("4");
		r1.setAdultsCount("2");
		r1.setChildCount("1");
		String[] namearray1 = new String[3];
		namearray1[0] = "{Mr_Akila Samaranayake";
		namearray1[1] = "MS_Rasoja Samaranayake";
		namearray1[2] = "Child_Sumiuru Gihan}";
		r1.setAdultsOccpancy(namearray1);
		
		String[] childAges1 = new String[1];
		childAges1[0] = "9";
		r1.setChildAges(childAges1);

		r2.setRoomNo("1");
		r2.setRoomCode("99171");
		r2.setRoomTypeID("3");
		r2.setRatePlanCode("Room Only");
		r2.setBedTypeID("4");
		r2.setAdultsCount("2");
		r2.setChildCount("0");
		String[] namearray3 = new String[2];
		namearray3[0] = "Mr_Akila Samaranayake2";
		namearray3[1] = "MS_Rasoja Samaranayake2";
		r2.setAdultsOccpancy(namearray3);
		/*String[] namearray4 = new String[1];
		namearray4[0] = "Child_Sumiuru Gihan2";
		r2.setChildOccupancy(namearray4);
		String[] childAges2 = new String[1];
		childAges2[0] = "12";
		r2.setChildAges(childAges2);*/
	
		ArrayList<Room> roomList = new ArrayList<Room>();
		roomList.add(r1);
		roomList.add(r2);
		resObJ.setRoomList(roomList);
		resObJ.setUserComment("new Comment Teseting ");
	}

	@Test
	public void TestSuite() throws IOException {

		String URL = "Resources/SampleReservationRequest1.xml";
		String out = null;
		try {
			out = builderObj.buildRequest(URL, resObJ);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(out);
	}

	@After
	public void tearDown() {

		try {
			System.out.println("Test is Finshed ");
		} catch (Exception e) {
			System.out.println(e);
		}

	}
}
