package com.rezgateway.automation.pojo;

public class DailyRates {

	private String DailyCondition = "";
	private String Date = null;
	private double StdAdultRate =0.0;
	private double AdditionalAdultRate = 0.0;
	private double Total = 0.0;
	private String RateCode =  null;
	/**
	 * @return the dailyCondition
	 */
	public String getDailyCondition() {
		return DailyCondition;
	}
	/**
	 * @param dailyCondition the dailyCondition to set
	 */
	public void setDailyCondition(String dailyCondition) {
		DailyCondition = dailyCondition;
	}
	/**
	 * @return the date
	 */
	public String getDate() {
		return Date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		Date = date;
	}
	/**
	 * @return the stdAdultRate
	 */
	public double getStdAdultRate() {
		return StdAdultRate;
	}
	/**
	 * @param stdAdultRate the stdAdultRate to set
	 */
	public void setStdAdultRate(double stdAdultRate) {
		StdAdultRate = stdAdultRate;
	}
	/**
	 * @return the additionalAdultRate
	 */
	public double getAdditionalAdultRate() {
		return AdditionalAdultRate;
	}
	/**
	 * @param additionalAdultRate the additionalAdultRate to set
	 */
	public void setAdditionalAdultRate(double additionalAdultRate) {
		AdditionalAdultRate = additionalAdultRate;
	}
	/**
	 * @return the total
	 */
	public double getTotal() {
		return Total;
	}
	/**
	 * @param total the total to set
	 */
	public void setTotal(double total) {
		Total = total;
	}
	/**
	 * @return the rateCode
	 */
	public String getRateCode() {
		return RateCode;
	}
	/**
	 * @param rateCode the rateCode to set
	 */
	public void setRateCode(String rateCode) {
		RateCode = rateCode;
	}
	
}
