package com.rezgateway.automation.xmlout.hotel;

import java.io.IOException;
import java.util.Arrays;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.rezgateway.automation.JavaHttpHandler;
import com.rezgateway.automation.builder.request.AvailabilityRequestBuilder;
import com.rezgateway.automation.input.ExcelReader;
import com.rezgateway.automation.pojo.AvailabilityRequest;
import com.rezgateway.automation.pojo.AvailabilityResponse;
import com.rezgateway.automation.pojo.HttpResponse;
import com.rezgateway.automation.reader.response.AvailabilityResponseReader;
import com.rezgateway.automation.reports.ExtentTestNGReportBuilderExt;
import com.rezgateway.automation.xmlout.utill.DataLoader;

public class BonoAvailabilityTest extends ExtentTestNGReportBuilderExt {

	private static int senarioID = 1;

	@Test(dataProvider = "getAvailabilityData")
	public synchronized void HotelAvailabilityTest(AvailabilityRequest Request) throws IOException {

		ITestResult result = Reporter.getCurrentTestResult();
		String testname = "Test number  " + senarioID + " : Search By : " + Request.getSearchType() + " Code : " + Arrays.toString(Request.getCode());
		result.setAttribute("TestName", testname);
		result.setAttribute("Expected", "Results Should be available");

		JavaHttpHandler handler = new JavaHttpHandler();

		HttpResponse Response = handler.sendPOST("http://192.168.1.83:8080/bonotelapps/bonotel/reservation/GetAvailability.do",
				"xml=" + new AvailabilityRequestBuilder().buildRequest("Resources/Sample_AvailRequestNew_senarioID_" + senarioID + ".xml", Request));
		senarioID++;
		if (Response.getRESPONSE_CODE() == 200) {
			AvailabilityResponse AvailabilityResponse = new AvailabilityResponseReader().getResponse(Response.getRESPONSE());
			if (AvailabilityResponse.getHotelCount() > 0) {
				result.setAttribute("Actual", "Results Available, Hotel Count :" + AvailabilityResponse.getHotelCount());
			} else {
				result.setAttribute("Actual", "Results not available Error Code :" + AvailabilityResponse.getErrorCode() + " Error Desc :" + AvailabilityResponse.getErrorDescription());
				Assert.fail("No Results Error Code :" + AvailabilityResponse.getErrorCode());
			}

		} else {
			result.setAttribute("Actual", "No Response recieved Code :" + Response.getRESPONSE_CODE());
			Assert.fail("Invalid Response Code :" + Response.getRESPONSE_CODE() + " ,No Response received");
		}
	}

	@DataProvider(name = "getAvailabilityData")
	public AvailabilityRequest[][] getAvailabilityData() throws Exception {

		ExcelReader reader = new ExcelReader();
		DataLoader loader = new DataLoader();

		return loader.getAvailabilityObjList(reader.getExcelData("Resources/HotelScenarios.xls", "Availability"));

	}

}
