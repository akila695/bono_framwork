package com.rezgateway.automation;

import com.rezgateway.automation.enu.RequestType;
import com.rezgateway.automation.pojo.HttpResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class JavaHttpHandler {
	private String USER_AGENT = "Mozilla/5.0";

	public HttpResponse sendGET(String URL) throws IOException {
		HttpResponse httpresponse = new HttpResponse();
		HttpURLConnection con = this.getConnection(URL, RequestType.GET);
		int responseCode = con.getResponseCode();
		httpresponse.setRESPONSE_CODE(responseCode);
		System.out.println("GET Response Code :: " + responseCode);
		if (responseCode != 200) {
			System.out.println("GET request not worked");
			httpresponse.setRESPONSE("N/A");
			return httpresponse;
		} else {
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			StringBuffer response = new StringBuffer();

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}

			in.close();
			httpresponse.setRESPONSE(response.toString());
			return httpresponse;
		}
	}

	public HttpResponse sendPOST(String URL, String Message) throws IOException {
		HttpResponse httpresponse = new HttpResponse();
		HttpURLConnection con = this.getConnection(URL, RequestType.POST);
		con.setDoOutput(true);
		OutputStream os = con.getOutputStream();
		os.write(Message.getBytes());
		os.flush();
		os.close();
		int responseCode = con.getResponseCode();
		httpresponse.setRESPONSE_CODE(responseCode);
		System.out.println("POST Response Code :: " + responseCode);
		if (responseCode != 200) {
			System.out.println("POST request not worked");
			httpresponse.setRESPONSE("N/A");
			return httpresponse;
		} else {
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			StringBuffer response = new StringBuffer();

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}

			in.close();
			httpresponse.setRESPONSE(response.toString());
			return httpresponse;
		}
	}

	public HttpResponse sendGET(HttpURLConnection con) throws IOException {
		HttpResponse httpresponse = new HttpResponse();
		int responseCode = con.getResponseCode();
		httpresponse.setRESPONSE_CODE(responseCode);
		System.out.println("GET Response Code :: " + responseCode);
		if (responseCode != 200) {
			System.out.println("GET request not worked");
			httpresponse.setRESPONSE("N/A");
			return httpresponse;
		} else {
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			StringBuffer response = new StringBuffer();

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}

			in.close();
			httpresponse.setRESPONSE(response.toString());
			return httpresponse;
		}
	}

	public HttpResponse sendPOST(HttpURLConnection con, String Message)
			throws IOException {
		HttpResponse httpresponse = new HttpResponse();
		con.setDoOutput(true);
		OutputStream os = con.getOutputStream();
		os.write(Message.getBytes());
		os.flush();
		os.close();
		int responseCode = con.getResponseCode();
		httpresponse.setRESPONSE_CODE(responseCode);
		System.out.println("POST Response Code :: " + responseCode);
		if (responseCode != 200) {
			System.out.println("POST request not worked");
			httpresponse.setRESPONSE("N/A");
			return httpresponse;
		} else {
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			StringBuffer response = new StringBuffer();

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}

			in.close();
			httpresponse.setRESPONSE(response.toString());
			return httpresponse;
		}
	}

	public HttpURLConnection getConnection(String URL, RequestType requesttype)
			throws IOException {
		URL obj = new URL(URL);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod(requesttype == RequestType.POST ? "POST" : "GET");
		con.setRequestProperty("User-Agent", this.USER_AGENT);
		return con;
	}
}