package com.rezgateway.automation.builder.request;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.rezgateway.automation.pojo.CancellationRequest;

public class CancellationRequestBuilderTest {

	CancellationRequest 			resObJ 			= 	new CancellationRequest();
	CancellationRequestBuilder 		buildReqObj 	= 	new CancellationRequestBuilder();

	@Before
	public void Setup() {

		resObJ.setUserName("TesthotelName");
		resObJ.setPassword("Akila");
		LocalDateTime date = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
		//LocalDate parsedDate = LocalDate.parse(text, formatter);
		resObJ.setCancellationRequestTimestamp(date.format(formatter));
		resObJ.setSupplierReferenceNo("362528317X");
		resObJ.setCancellationNotes("Automation");
		resObJ.setCancellationReason("Automation_Test");

	}

	@Test
	public void TestSuite() throws IOException {

		String URL = "Resources/Sample_Cancellation_Request.xml";
		String out = null;
		
		try {
			out = buildReqObj.buildRequest(URL, resObJ);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(out);
	}

	@Test
	public void TestSuite2(){
		
		String out = null;
		try {
			out = buildReqObj.buildRequest(resObJ);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(out);
	}
	
	
	@After
	public void tearDown() {

		try {
			System.out.println("Test is Finshed ");
		} catch (Exception e) {
			System.out.println(e);
		}

	}

}
