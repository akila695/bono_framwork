package com.rezgateway.automation.builder.request;

import java.io.FileWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.jdom2.Element;
import org.jdom2.Document;
import org.jdom2.Attribute;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import com.rezgateway.automation.enu.ConfirmationType;
import com.rezgateway.automation.enu.ModificationType;
import com.rezgateway.automation.pojo.ModificationRequest;
import com.rezgateway.automation.pojo.Room;

public class ModificationRequestBuilder {

	private static Logger logger = null;

	public ModificationRequestBuilder() {
		logger = Logger.getLogger(ModificationRequestBuilder.class);
	}

	public Document getModifyTypeOption(ModificationRequest reqObj) throws Exception {

		if (ModificationType.GUESTDETAILS == reqObj.getModifyType()) {
			return getDocumentForModifyReservastion(reqObj);
		} else if (ModificationType.AMENDNOTE == reqObj.getModifyType()) {
			return getDocumentForModifyNote(reqObj);
		} else if (ModificationType.STAYPERIOD == reqObj.getModifyType()) {
			return getDocumentForModifyReservastion(reqObj);
		} else if (ModificationType.AMENDROOM == reqObj.getModifyType()) {
			return getDocumentForModifyReservastion(reqObj);
		} else if (ModificationType.ADDROOM == reqObj.getModifyType()) {
			return getDocumentForModifyReservastion(reqObj);
		} else if (ModificationType.TOUROPERAORNUM == reqObj.getModifyType()) {
			return getDocumentForModifyNote(reqObj);
		}else{
			logger.error("User Modify Selection is Wrong Please Enter relevant Modification Type ");
			throw new Exception("User Modify Selection is Wrong Please Enter relevant Modification Type ");
		}
		
		
	}

	public Document getDocumentForModifyReservastion(ModificationRequest reqObj) {

		Document doc = new Document();
		Element modifyReservationRequest = new Element("modifyReservationRequest");

		try {
			logger.info("........" + reqObj.getModifyType().toString() + "_modifyReservationRequest__Document Starting..........");
			doc.setRootElement(modifyReservationRequest);
			Element controls = new Element("control");
			controls.addContent(new Element("username").setText(reqObj.getUserName()));
			controls.addContent(new Element("password").setText(reqObj.getPassword()));
			doc.getRootElement().addContent(controls);
			Element modifyReservationDetails = new Element("modifyReservationDetails");
			modifyReservationDetails.setAttribute("timeStamp", reqObj.getModificationDetailsTimeStamp());
			modifyReservationDetails.setAttribute("modifyType", reqObj.getModifyType().toString());

			if (ConfirmationType.CON == reqObj.getConfType()) {
				modifyReservationDetails.addContent(new Element("confirmationType").setText("CON"));
			} else if (ConfirmationType.REQ == reqObj.getConfType()) {
				modifyReservationDetails.addContent(new Element("confirmationType").setText("REQ"));
			} else {
				logger.error("........ERROR_ConfType_Selection..........");
				modifyReservationDetails.addContent(new Element("confirmationType").setText("ERROR_ConfType_Selection"));
			}
			modifyReservationDetails.addContent(new Element("tourOperatorOrderNumber").setText(reqObj.getTourOperatorOrderNumber()));
			modifyReservationDetails.addContent(new Element("checkIn").setText(reqObj.getCheckin()));
			modifyReservationDetails.addContent(new Element("CheckOUT").setText(reqObj.getCheckout()));
			modifyReservationDetails.addContent(new Element("noOfRooms").setText(Integer.toString((reqObj.getRoomList().size()))));
			modifyReservationDetails.addContent(new Element("noOfNights").setText(reqObj.getNoofNights()));
			modifyReservationDetails.addContent(new Element("hotelCode").setText(reqObj.getHotelCode()));
			modifyReservationDetails.addContent(new Element("total").setAttribute("currency", reqObj.getCurrency()).setText(reqObj.getTotal()));
			modifyReservationDetails.addContent(new Element("totalTax").setAttribute("currency", reqObj.getCurrency()).setText(reqObj.getTotalaTax()));

			
			// //
			if ((reqObj.getRoomList().size() > 0) && (reqObj.getRoomList() != null)) {
				
								
				for (Room r : reqObj.getRoomList()) {
					
					Element roomData = new Element("roomData");
					modifyReservationDetails.addContent(roomData);
					roomData.addContent(new Element("roomNo").setText(r.getRoomNo()));
					roomData.addContent(new Element("roomCode").setText(r.getRoomCode()));
					roomData.addContent(new Element("roomTypeCode").setText(r.getRoomTypeID()));
					roomData.addContent(new Element("bedTypeCode").setText(r.getBedTypeID()));
					roomData.addContent(new Element("ratePlanCode").setText(r.getRatePlanCode()));
					roomData.addContent(new Element("noOfAdults").setText(r.getAdultsCount()));
					roomData.addContent(new Element("noOfChildren").setText(r.getChildCount()));

					// //Note >>>>>> Last name and First name Counts Should be Equal to adults+Child Count
					int paxCount = (Integer.parseInt((r.getAdultsCount()))) + (Integer.parseInt((r.getChildCount())));
					Element occupancy = new Element("occupancy");

					if (paxCount != 0) {
						roomData.addContent(occupancy);
						
						for(int i = 0; i < (r.getAdultsOccpancy().length); i++) {
							
							Element guest = new Element("guest");
							String[] adultnamesList = r.getAdultsOccpancy();
							String[] fullName = adultnamesList[i].split("_");
							guest.addContent(new Element("title").setText(fullName[0]));
							guest.addContent(new Element("firstName").setText(fullName[1]));
							guest.addContent(new Element("lastname").setText(" Adult_" + (i + 1) + "_Lastname"));
							occupancy.addContent(guest);

						}
						if (!("0").equals(r.getChildCount())) {
							for (int i = 0; i < (r.getChildOccupancy().length); i++) {
								
								String[] childnameList = r.getChildOccupancy();
								String[] childAgesList = r.getChildAges();
								Element guest = new Element("guest");
								String[] fullName = childnameList[i].split("_");
								guest.addContent(new Element("title").setText(fullName[0]));
								guest.addContent(new Element("firstName").setText(fullName[1]));
								guest.addContent(new Element("lastname").setText(" Child_" + (i + 1) + "_Lastname"));
								guest.addContent(new Element("Age").setText(childAgesList[i]));
								occupancy.addContent(guest);

							}
						}else{
							logger.info("........Child count is Zero..........");
							
						}
						
					} else {
						logger.error("........Error PaxCount entered..........");
						roomData.addContent(occupancy).setText("Error PaxCount entered");
						doc.getRootElement().addContent(modifyReservationDetails);
						// throw new Exception(" name Count and PAX count is not matched ");
					}
					
				}
				
			} else {
				logger.error("........No Rooms Availble for This hotel or No Rooom Data Provided..........");
				modifyReservationDetails.addContent("error").setText("No Rooms Availble for This hotel or No Rooom Data Provided");
				doc.getRootElement().addContent(modifyReservationDetails);
			}
			
			
			Element comment = new Element("comment");
			modifyReservationDetails.addContent(comment);
			comment.addContent(new Element("Customer").setText(reqObj.getUserComment()));
			comment.addContent(new Element("Hotel").setText(reqObj.getHotelComment()));
			doc.getRootElement().addContent(modifyReservationDetails);
			logger.info("........" + reqObj.getModifyType().toString() + "_modifyReservationRequest__Document Ending..........");
			
		} catch (Exception e) {
			logger.fatal("Error While Building Doc :", e);
			e.printStackTrace();
		}

		return doc;

	}

	public Document getDocumentForModifyNote(ModificationRequest reqObj) {

		Document doc = new Document();
		Element modifyReservationRequest = new Element("modifyReservationRequest");

		try {
			logger.info("........" + reqObj.getModifyType().toString() + "_modifyReservationRequest__Document Starting..........");
			modifyReservationRequest.setAttribute(new Attribute("Version", reqObj.getVersion()));
			doc.setRootElement(modifyReservationRequest);
			Element controls = new Element("control");
			controls.addContent(new Element("username").setText(reqObj.getUserName()));
			controls.addContent(new Element("password").setText(reqObj.getPassword()));
			doc.getRootElement().addContent(controls);
			Element modifyReservationDetails = new Element("modifyReservationDetails");
			modifyReservationDetails.setAttribute("timeStamp", reqObj.getModificationDetailsTimeStamp());
			modifyReservationDetails.setAttribute("modifyType", reqObj.getModifyType().toString());
			modifyReservationDetails.addContent(new Element("ReferenceNo").setText(reqObj.getReferenceNo()));

			if (ConfirmationType.CON == reqObj.getConfType()) {
				modifyReservationDetails.addContent(new Element("confirmationType").setText("CON"));
			} else if (ConfirmationType.REQ == reqObj.getConfType()) {
				modifyReservationDetails.addContent(new Element("confirmationType").setText("REQ"));
			} else {
				logger.error("........ERROR_ConfType_Selection..........");
				modifyReservationDetails.addContent(new Element("confirmationType").setText("ERROR_ConfType_Selection"));
			}
			modifyReservationDetails.addContent(new Element("tourOperatorOrderNumber").setText(reqObj.getTourOperatorOrderNumber()));
			Element comment = new Element("comment");
			modifyReservationDetails.addContent(comment);
			comment.addContent(new Element("Customer").setText(reqObj.getUserComment()));
			comment.addContent(new Element("Hotel").setText(reqObj.getHotelComment()));
			modifyReservationRequest.addContent(modifyReservationDetails);
			logger.info("........" + reqObj.getModifyType().toString() + "_modifyReservationRequest__Document Ending..........");
		} catch (Exception e) {
			logger.fatal("Error While Building Document :", e);
			e.printStackTrace();
		}

		return doc;

	}

	
	public String buildRequest(String Path, ModificationRequest resObj) throws Exception {

		StringWriter sw = new StringWriter();
		String res = null;
		Document xmlDoc = null;
		try {
			xmlDoc = getModifyTypeOption(resObj);
			XMLOutputter xmlOutput = new XMLOutputter();
			xmlOutput.setFormat(Format.getPrettyFormat());
			xmlOutput.output(xmlDoc, new FileWriter(Path));
			xmlOutput.output(xmlDoc, sw);
			res = sw.getBuffer().toString();
		} catch (Exception es) {
			logger.fatal("Error While Building Document :", es);
			System.out.println(es);
			es.printStackTrace();
		}
		return res;
	}

	public String buildRequest(ModificationRequest resObj) {

		StringWriter sw = new StringWriter();
		String res = null;
		Document xmlDoc = null;
		try {
			xmlDoc = getModifyTypeOption(resObj);
			XMLOutputter xmlOutput = new XMLOutputter();
			xmlOutput.setFormat(Format.getPrettyFormat());
			xmlOutput.output(xmlDoc, sw);
			res = sw.getBuffer().toString();
		} catch (Exception es) {
			logger.fatal("Error While Building Document :", es);
			System.out.println(es);
			es.printStackTrace();
		}
		return res;

	}

}
