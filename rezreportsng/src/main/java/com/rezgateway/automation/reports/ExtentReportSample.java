package com.rezgateway.automation.reports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

/**
 * Hello world!
 *
 */
public class ExtentReportSample 
{
    public static void main( String[] args )
    {
        ExtentHtmlReporter  reporter = new ExtentHtmlReporter("Resource/sample.html");
        ExtentReports       extent   = new ExtentReports();
        
        extent.attachReporter(reporter);
        
        ExtentTest ParentTest = extent.createTest("Hotel Setup");
        ExtentTest ChildTest  = ParentTest.createNode("Standard Setup");
        
        ChildTest.info("Expected  : M");
        ChildTest.info("Available : N");
        
        ChildTest.fail("Test Failed");
        ParentTest.createNode("Rooms Setup").info("Expectet : k").info("Availanle : K").pass("Test Pass");
        extent.flush();
        
        
        
        
    }
}
