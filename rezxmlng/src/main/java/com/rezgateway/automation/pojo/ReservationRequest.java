package com.rezgateway.automation.pojo;

import java.util.ArrayList;

import com.rezgateway.automation.enu.ConfirmationType;

public class ReservationRequest {
	
	private String 				ReturnCompeleteBookingDetails 			= "Y";
	private String 				UserName 								= "TestDynamicRateTO";
	private String 				Password 								= "test12345";
	private String				ReservationDetailsTimeStamp				= null;
	private String 				TourOperatorOrderNumber					= null;
	private String 				Checkin 								= null;
	private String 				Checkout 								= null;
	private String 				NoOfRooms								= null;
	private String 				NoofNights 								= null;
	private String 				HotelCode								= "6109";
	private String 				Total 									= null;
	private String 				TotalaTax								= null;
	private String	 			Currency								= null;
	private ArrayList<Room>     RoomList								= new ArrayList<Room>();
	private ConfirmationType    ConfType								= ConfirmationType.CON;
	private String 				UserComment								= "No comments available";
	private String 				HotelComment							= "No comments available";
	
	
	public ReservationRequest getDefaultValue() {

		ReservationRequest reqObJ = new ReservationRequest();
		Room r1 = new Room();
		
		reqObJ.setUserName(UserName);
		reqObJ.setPassword(Password);
		reqObJ.setReservationDetailsTimeStamp("20090925T11:19:36");
		reqObJ.setConfType("CON");
		reqObJ.setTourOperatorOrderNumber("20113217T03:32:20");
		reqObJ.setCheckin("24-MAR-2018");
		reqObJ.setCheckout("25-MAR-2018");
		reqObJ.setNoofNights("1");
		reqObJ.setHotelCode(HotelCode);
		reqObJ.setCurrency("USD");
		reqObJ.setTotal("0");
		reqObJ.setTotalaTax("0");

		r1.setRoomNo("1");
		r1.setRoomCode("99171");
		r1.setRoomTypeID("3");
		r1.setRatePlanCode("1");
		r1.setBedTypeID("4");
		r1.setAdultsCount("2");
		r1.setChildCount("1");
		String[] namearray1 = new String[2];
		namearray1[0] = "Mr_Akila Samaranayake";
		namearray1[1] = "MS_Rasoja Samaranayake";
		r1.setAdultsOccpancy(namearray1);
		String[] namearray2 = new String[1];
		namearray2[0] = "Child_Sumiuru Gihan";
		r1.setChildOccupancy(namearray2);

		String[] childAges1 = new String[1];
		childAges1[0] = "9";
		r1.setChildAges(childAges1);
		
		ArrayList<Room> roomList = new ArrayList<Room>();
		roomList.add(r1);
		reqObJ.setRoomList(roomList);
		reqObJ.setUserComment("new Comment Teseting ");
		
		return reqObJ;
	}

	/**
	 * @return the returnCompeleteBookingDetails
	 */
	public String getReturnCompeleteBookingDetails() {
		return ReturnCompeleteBookingDetails;
	}
	/**
	 * @param returnCompeleteBookingDetails the returnCompeleteBookingDetails to set
	 */
	public void setReturnCompeleteBookingDetails(String returnCompeleteBookingDetails) {
		ReturnCompeleteBookingDetails = returnCompeleteBookingDetails;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return UserName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		UserName = userName;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return Password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		Password = password;
	}
	/**
	 * @return the reservationDetailsTimeStamp
	 */
	public String getReservationDetailsTimeStamp() {
		return ReservationDetailsTimeStamp;
	}
	/**
	 * @param reservationDetailsTimeStamp the reservationDetailsTimeStamp to set
	 */
	public void setReservationDetailsTimeStamp(String reservationDetailsTimeStamp) {
		ReservationDetailsTimeStamp = reservationDetailsTimeStamp;
	}
	/**
	 * @return the tourOperatorOrderNumber
	 */
	public String getTourOperatorOrderNumber() {
		return TourOperatorOrderNumber;
	}
	/**
	 * @param tourOperatorOrderNumber the tourOperatorOrderNumber to set
	 */
	public void setTourOperatorOrderNumber(String tourOperatorOrderNumber) {
		TourOperatorOrderNumber = tourOperatorOrderNumber;
	}
	/**
	 * @return the checkin
	 */
	public String getCheckin() {
		return Checkin;
	}
	/**
	 * @param checkin the checkin to set
	 */
	public void setCheckin(String checkin) {
		Checkin = checkin;
	}
	/**
	 * @return the checkout
	 */
	public String getCheckout() {
		return Checkout;
	}
	/**
	 * @param checkout the checkout to set
	 */
	public void setCheckout(String checkout) {
		Checkout = checkout;
	}
	/**
	 * @return the noofNights
	 */
	public String getNoofNights() {
		return NoofNights;
	}
	/**
	 * @param noofNights the noofNights to set
	 */
	public void setNoofNights(String noofNights) {
		NoofNights = noofNights;
	}
	/**
	 * @return the hotelCode
	 */
	public String getHotelCode() {
		return HotelCode;
	}
	/**
	 * @param hotelCode the hotelCode to set
	 */
	public void setHotelCode(String hotelCode) {
		HotelCode = hotelCode;
	}
	/**
	 * @return the total
	 */
	public String getTotal() {
		return Total;
	}
	/**
	 * @param total the total to set
	 */
	public void setTotal(String total) {
		Total = total;
	}
	/**
	 * @return the totalaTax
	 */
	public String getTotalaTax() {
		return TotalaTax;
	}
	/**
	 * @param totalaTax the totalaTax to set
	 */
	public void setTotalaTax(String totalaTax) {
		TotalaTax = totalaTax;
	}
	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return Currency;
	}
	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		Currency = currency;
	}
	/**
	 * @return the roomList
	 */
	public ArrayList<Room> getRoomList() {
		return RoomList;
	}
	/**
	 * @param roomList the roomList to set
	 */
	public void setRoomList(ArrayList<Room> roomList) {
		RoomList = roomList;
	}
	/**
	 * @return the confType
	 */
	public ConfirmationType getConfType() {
		return ConfType;
	}
	/**
	 * @param confType the confType to set
	 */
	public void setConfType(ConfirmationType confType) {
		ConfType = confType;
	}
	
	public void setConfType(String confType){
		
		ConfType = ConfirmationType.getConfirmationType(confType);
	}
	/**
	 * @return the userComment
	 */
	public String getUserComment() {
		return UserComment;
	}
	/**
	 * @param userComment the userComment to set
	 */
	public void setUserComment(String userComment) {
		UserComment = userComment;
	}
	/**
	 * @return the hotelComment
	 */
	public String getHotelComment() {
		return HotelComment;
	}
	/**
	 * @param hotelComment the hotelComment to set
	 */
	public void setHotelComment(String hotelComment) {
		HotelComment = hotelComment;
	}

	/**
	 * @return the noOfRooms
	 */
	public String getNoOfRooms() {
		return NoOfRooms;
	}

	/**
	 * @param noOfRooms the noOfRooms to set
	 */
	public void setNoOfRooms(String noOfRooms) {
		NoOfRooms = noOfRooms;
	}
	
	
	
	
}
