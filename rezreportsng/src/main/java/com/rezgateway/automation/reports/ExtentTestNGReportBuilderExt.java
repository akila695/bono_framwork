package com.rezgateway.automation.reports;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentTestNGReportBuilderExt {

	private static ExtentReports extent;
	@SuppressWarnings("rawtypes")
	private static ThreadLocal parentTest = new ThreadLocal();
	@SuppressWarnings("rawtypes")
	private static ThreadLocal test = new ThreadLocal();

	@BeforeSuite
	public synchronized void beforeSuite() {
		extent = ExtentManager.createInstance("ExtentReport.html");
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("ExtentReport.html");
		extent.attachReporter(htmlReporter);
	}

	@SuppressWarnings("unchecked")
	@BeforeClass
	public synchronized void beforeClass() {
		ExtentTest parent = extent.createTest(getClass().getName().substring(getClass().getName().lastIndexOf(".") + 1));
		parentTest.set(parent);
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @BeforeMethod public synchronized void beforeMethod(Method method) { ExtentTest child = ((ExtentTest) parentTest.get()).createNode(method.getName().substring(method.getName().lastIndexOf(".") +
	 * 1));
	 * 
	 * test.set(child); }
	 */
	@SuppressWarnings("unchecked")
	@AfterMethod
	public synchronized void afterMethod(ITestResult result) {

		ExtentTest child = ((ExtentTest) parentTest.get()).createNode(result.getAttribute("TestName").toString());
		child.info("Expected :" + result.getAttribute("Expected").toString());
		child.info("Actual   :" + result.getAttribute("Actual").toString());
		test.set(child);
		if (result.getStatus() == ITestResult.FAILURE)
			((ExtentTest) test.get()).fail(result.getThrowable());
		else if (result.getStatus() == ITestResult.SKIP)
			((ExtentTest) test.get()).skip(result.getThrowable());
		else
			((ExtentTest) test.get()).pass("Test passed");

		extent.flush();
	}
}
