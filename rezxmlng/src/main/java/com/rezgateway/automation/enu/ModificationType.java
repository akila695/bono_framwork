/**
 * 
 */
package com.rezgateway.automation.enu;

/**
 * @author Akila
 *
 */
public enum ModificationType {
	TOUROPERAORNUM, ADDROOM, AMENDROOM, STAYPERIOD, AMENDNOTE, GUESTDETAILS, NONE;

	public static ModificationType getModifyType(String type) {

		if (type.trim().equals("1")) {
			return ModificationType.TOUROPERAORNUM;
		} else if (type.trim().equals("2")) {
			return ModificationType.ADDROOM;
		} else if (type.trim().equals("3")) {
			return ModificationType.AMENDROOM;
		} else if (type.trim().equals("4")) {
			return ModificationType.STAYPERIOD;
		} else if (type.trim().equals("5")) {
			return ModificationType.AMENDNOTE;
		} else if (type.trim().equals("6")) {
			return ModificationType.GUESTDETAILS;
		} else {
			return ModificationType.NONE;
		}

	}

}
